'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('genre', [{
      id: '36094ee0-fb6a-4a4b-9e25-9fc9967e9672',
      categoryId: 'bdfbd3b4-14bd-41b1-9f35-0c1298d85908',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'script-fill',
      name: 'Script_Fill',
    }, {
      id: '7799bd9d-8d38-4248-960e-85bbe19fa47d',
      categoryId: 'bdfbd3b4-14bd-41b1-9f35-0c1298d85908',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'improv',
      name: 'Improv',
    }, {
      id: '72953b91-ed90-488f-8e07-0360b6ba8143',
      categoryId: 'bdfbd3b4-14bd-41b1-9f35-0c1298d85908',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'original',
      name: 'Original',
    }, {
      id: 'fbe9af40-6435-4a8f-bef3-a8fd7119b5c8',
      categoryId: 'bdfbd3b4-14bd-41b1-9f35-0c1298d85908',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'ramblefap',
      name: 'Ramblefap',
    }, {
      id: '69abfc0b-df54-4385-9696-13457a6446bf',
      categoryId: 'bdfbd3b4-14bd-41b1-9f35-0c1298d85908',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'narrative',
      name: 'Narrative',
    }, {
      id: '540a576b-4e46-4b91-97e6-0798a7ae2349',
      categoryId: 'bdfbd3b4-14bd-41b1-9f35-0c1298d85908',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'instructional',
      name: 'Instructional',
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
