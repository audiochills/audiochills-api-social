'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkUpdate('genre', {
      slug: 'roleplay',
      name: 'Roleplay',
    }, {
      id: '57f8751c-1fd1-4b27-96ca-aba207971067',
    });
    await queryInterface.bulkUpdate('genre', {
      slug: 'massage',
      name: 'Massage',
    }, {
      id: '44e2e7ed-4203-4aac-af85-f77bf741ac7c',
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkUpdate('genre', {
      slug: 'roleplay-sfw',
      name: 'Roleplay SFW',
    }, {
      id: '57f8751c-1fd1-4b27-96ca-aba207971067',
    });
    await queryInterface.bulkUpdate('genre', {
      slug: 'massage-sfw',
      name: 'Massage_SFW',
    }, {
      id: '44e2e7ed-4203-4aac-af85-f77bf741ac7c',
    });
  }
};
