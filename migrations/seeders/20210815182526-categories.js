'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('category', [{
      id: '2c3a1d05-1248-44f1-97b8-503a00ef61d5',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'music',
      name: 'Music',
    }, {
      id: 'bdfbd3b4-14bd-41b1-9f35-0c1298d85908',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'voice-art',
      name: 'Voice Art',
    }, {
      id: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'asmr-sfw',
      name: 'ASMR SFW',
    }, {
      id: '4517741b-deaa-4ff7-a03f-a7facc077f4d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'asmr-nsfw',
      name: 'ASMR NSFW',
    }, {
      id: '18e4fcae-cb4d-4970-bc72-a04295657b79',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'readings',
      name: 'Readings',
    }, {
      id: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'audio-drama',
      name: 'Audio Drama',
    }, {
      id: '3134c2e2-ee6f-4321-92fd-b9b10f49bc7d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'community',
      name: 'Community',
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
