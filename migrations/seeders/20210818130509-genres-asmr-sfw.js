'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('genre', [{
      id: 'fe4c051b-a381-4ec8-b76d-b9b9ff9e1d86',
      categoryId: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'personal-attention',
      name: 'Personal Attention',
    }, {
      id: '57f8751c-1fd1-4b27-96ca-aba207971067',
      categoryId: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'roleplay-sfw',
      name: 'Roleplay SFW',
    }, {
      id: '328299d2-13fb-4c23-878d-123692e41af2',
      categoryId: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'page-turning',
      name: 'Page Turning',
    }, {
      id: '3369293a-3fcd-4d47-9f09-e151c7295c80',
      categoryId: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'whispering',
      name: 'Whispering',
    }, {
      id: 'be8b850c-b545-4d2c-838d-b2012e5c44c1',
      categoryId: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'concentration',
      name: 'Concentration',
    }, {
      id: 'cd66cb91-283b-4b2a-b527-36e6f6a46150',
      categoryId: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'tactile',
      name: 'Tactile',
    }, {
      id: 'd7c16828-c3e6-4528-992b-5be32b7328e7',
      categoryId: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'sleep-aid',
      name: 'Sleep_Aid',
    }, {
      id: 'b4428891-f732-405e-a2fe-94a045386f12',
      categoryId: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'food',
      name: 'Food',
    }, {
      id: '44e2e7ed-4203-4aac-af85-f77bf741ac7c',
      categoryId: 'da0ef822-e4e1-4c79-8948-0c4717007748',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'massage-sfw',
      name: 'Massage_SFW',
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
