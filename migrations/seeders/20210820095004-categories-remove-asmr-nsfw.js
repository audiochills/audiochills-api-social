'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('category', { id: [
      '4517741b-deaa-4ff7-a03f-a7facc077f4d',
    ]}, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('category', [{
      id: '4517741b-deaa-4ff7-a03f-a7facc077f4d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'asmr-nsfw',
      name: 'ASMR NSFW',
    }], {});
  }
};
