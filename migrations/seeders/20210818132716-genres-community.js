'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('genre', [{
      id: '38e92205-cf47-483c-8ba4-ec8d21834457',
      categoryId: '3134c2e2-ee6f-4321-92fd-b9b10f49bc7d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'AMAs',
      name: 'AMAs',
    }, {
      id: '3622108d-c3b5-49a7-94dd-ca8d2680791a',
      categoryId: '3134c2e2-ee6f-4321-92fd-b9b10f49bc7d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'interviews',
      name: 'Interviews',
    }, {
      id: 'e57dfca2-4379-4a34-bc43-1de385fa83ee',
      categoryId: '3134c2e2-ee6f-4321-92fd-b9b10f49bc7d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'announcements',
      name: 'Announcements',
    }, {
      id: 'b66b6bcc-95e1-400f-91d6-2d416072f467',
      categoryId: '3134c2e2-ee6f-4321-92fd-b9b10f49bc7d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'messages',
      name: 'Messages',
    }, {
      id: '615c14b0-f06a-437a-b879-0a2f2ba8a59a',
      categoryId: '3134c2e2-ee6f-4321-92fd-b9b10f49bc7d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'teasers-and-previews',
      name: 'Teasers / Previews',
    }, {
      id: 'b9d12786-1341-49b0-a79c-efdd713c46a5',
      categoryId: '3134c2e2-ee6f-4321-92fd-b9b10f49bc7d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'miscellaneous',
      name: 'Miscellaneous',
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
