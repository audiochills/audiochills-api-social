'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkUpdate('category', {
      slug: 'asmr',
      name: 'ASMR',
    }, {
      id: 'da0ef822-e4e1-4c79-8948-0c4717007748',
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkUpdate('category', {
      slug: 'asmr-sfw',
      name: 'ASMR SFW',
    }, {
      id: 'da0ef822-e4e1-4c79-8948-0c4717007748',
    });
  }
};
