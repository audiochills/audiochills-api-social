'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('genre', [{
      id: '8302a2d9-3626-47f9-a4cd-a6f650404034',
      categoryId: '18e4fcae-cb4d-4970-bc72-a04295657b79',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'poetry',
      name: 'Poetry',
    }, {
      id: '3d8bb952-3918-42ef-819f-a10c104a5188',
      categoryId: '18e4fcae-cb4d-4970-bc72-a04295657b79',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'short-stories',
      name: 'Short_Stories',
    }, {
      id: 'b4f905cd-2d63-43cf-8fa9-ae378361fe53',
      categoryId: '18e4fcae-cb4d-4970-bc72-a04295657b79',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'literature',
      name: 'Literature',
    }, {
      id: '209d6f96-dba6-4278-a176-88fafa72f7f1',
      categoryId: '18e4fcae-cb4d-4970-bc72-a04295657b79',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'lyrical',
      name: 'Lyrical',
    }, {
      id: '2bf545e4-c7fa-42d4-8899-60d42be304ed',
      categoryId: '18e4fcae-cb4d-4970-bc72-a04295657b79',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'plays',
      name: 'Plays',
    }, {
      id: '2f78d67b-9dfd-4ec0-befa-a408cd54b6f4',
      categoryId: '18e4fcae-cb4d-4970-bc72-a04295657b79',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'essays-and-articles',
      name: 'Essays_/_Articles',
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
