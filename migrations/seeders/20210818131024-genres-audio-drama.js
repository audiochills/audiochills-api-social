'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('genre', [{
      id: '70b846e7-02c3-40a2-bd6f-d09d583da61c',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'adventure',
      name: 'Adventure',
    }, {
      id: 'fa1f39f4-f7b1-4bc7-8977-4d1a403d6bf3',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'anthology',
      name: 'Anthology',
    }, {
      id: '02a984da-22c2-4f8e-b9e4-f7b67688d8a2',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'comedy',
      name: 'Comedy',
    }, {
      id: 'b3f64c11-7ffd-46f9-99bd-ada04c986c4d',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'crime-and-murder',
      name: 'Crime and Murder',
    }, {
      id: '7977bb9b-3d6e-4f66-bd5d-7859c2cb7fee',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'general-audiodrame',
      name: 'General Audiodrame',
    }, {
      id: '4a66ada4-3992-4d03-8635-2cb3198f599e',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'family-friendly',
      name: 'Family Friendly',
    }, {
      id: '0d12a973-68fe-4960-803c-0fb69a5d6459',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'fan-fiction',
      name: 'Fan Fiction',
    }, {
      id: '22cfe1a4-4144-451e-8b6f-ca6ec8547e9b',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'fantasy',
      name: 'Fantasy',
    }, {
      id: 'cf47de59-4ca3-4e7c-b250-55e3f840927f',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'fables-and-folklore',
      name: 'Fables and Folklore',
    }, {
      id: '93adaa34-744f-42b9-8983-9641be74f3f3',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'historical-alternate-history',
      name: 'Historical / Alternate History',
    }, {
      id: '736d1b2f-333a-47d3-85ad-cac3a25a36c9',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'horrow',
      name: 'Horrow',
    }, {
      id: 'f2b5837e-5854-470d-816a-913fdc1d67c0',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'improvisational',
      name: 'Improvisational',
    }, {
      id: '220f3491-6c3a-4518-8ee0-49fdc1f02562',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'musical',
      name: 'Musical',
    }, {
      id: 'bbd1dcae-7ece-47aa-967e-318dc79290af',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'mystery',
      name: 'Mystery',
    }, {
      id: 'eb287659-c9a1-4b49-a770-4707f8623ee2',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'neo-futurist',
      name: 'Neo-Futurist',
    }, {
      id: 'd3e97102-fa16-4ec0-888f-004609322d92',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'political',
      name: 'Political',
    }, {
      id: 'a03f76c8-af3c-43de-a3ad-1f909b9d3ec1',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'science-fiction',
      name: 'Science_Fiction',
    }, {
      id: '44277175-8ae7-40ba-a332-256581ef98c4',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'thriller',
      name: 'Thriller',
    }, {
      id: 'd9a756ce-1b9d-4e01-af67-0858b8d838c8',
      categoryId: 'acfd1906-157a-4a1f-828d-79b40307e14a',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'young-adult',
      name: 'Young_Adult',
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
