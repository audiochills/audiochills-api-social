'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('genre', { id: [
      'f42692bd-6cb0-4d4f-a5ab-4f76da2d8e6d',
      'af75910c-73de-4d37-a293-ba1945bfbfeb',
      '9ae97e26-3bf6-4ba0-bbe7-162a3b81be34',
      '2408ad73-ca5c-4bd7-947e-b1094010bcd7',
      '3364d98b-dd7a-4533-810a-5604bf054752',
      '7e029294-8595-4ddc-94f1-e37b8f87606a',
    ]}, {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('genre', [{
      id: 'f42692bd-6cb0-4d4f-a5ab-4f76da2d8e6d',
      categoryId: '4517741b-deaa-4ff7-a03f-a7facc077f4d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'instructions',
      name: 'Instructions',
    }, {
      id: 'af75910c-73de-4d37-a293-ba1945bfbfeb',
      categoryId: '4517741b-deaa-4ff7-a03f-a7facc077f4d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'roleplay-nsfw',
      name: 'Roleplay NSFW',
    }, {
      id: '9ae97e26-3bf6-4ba0-bbe7-162a3b81be34',
      categoryId: '4517741b-deaa-4ff7-a03f-a7facc077f4d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'masturbation',
      name: 'Masturbation',
    }, {
      id: '2408ad73-ca5c-4bd7-947e-b1094010bcd7',
      categoryId: '4517741b-deaa-4ff7-a03f-a7facc077f4d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'licking',
      name: 'Licking',
    }, {
      id: '3364d98b-dd7a-4533-810a-5604bf054752',
      categoryId: '4517741b-deaa-4ff7-a03f-a7facc077f4d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'sensual',
      name: 'Sensual',
    }, {
      id: '7e029294-8595-4ddc-94f1-e37b8f87606a',
      categoryId: '4517741b-deaa-4ff7-a03f-a7facc077f4d',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'massage-nsfw',
      name: 'Massage NSFW',
    }], {});
  }
};
