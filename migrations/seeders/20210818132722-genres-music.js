'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('genre', [{
      id: '59b054d3-ca8f-44f8-bbae-5ce210844067',
      categoryId: '2c3a1d05-1248-44f1-97b8-503a00ef61d5',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'hip-hop',
      name: 'Hip/Hop',
    }, {
      id: '5bc8576d-58ed-40c6-bc2d-d5a612c79fc9',
      categoryId: '2c3a1d05-1248-44f1-97b8-503a00ef61d5',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'wlectronica',
      name: 'Electronica',
    }, {
      id: 'eb5b3f5b-d026-4079-9336-f00f14b6ac8e',
      categoryId: '2c3a1d05-1248-44f1-97b8-503a00ef61d5',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'jazz',
      name: 'Jazz',
    }, {
      id: '3dbfe589-ca16-4793-8c85-9a784dbbf51f',
      categoryId: '2c3a1d05-1248-44f1-97b8-503a00ef61d5',
      createdAt: new Date(),
      updatedAt: new Date(),
      slug: 'rock',
      name: 'Rock',
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
