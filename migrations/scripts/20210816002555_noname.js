const Sequelize = require("sequelize");

/**
 * Actions summary:
 *
 * createTable() => "profile", deps: []
 * createTable() => "category", deps: []
 * createTable() => "genre", deps: [category]
 * createTable() => "post", deps: [profile, genre]
 * createTable() => "profileFollowProfile", deps: [profile, profile]
 * createTable() => "comment", deps: [profile, post, comment, comment]
 * createTable() => "profileLikeComment", deps: [profile, comment]
 * createTable() => "profileHasCategory", deps: [profile, category]
 * createTable() => "profileLikePost", deps: [profile, post]
 * createTable() => "purchasePost", deps: [profile, post, profile, profile]
 * createTable() => "plan", deps: [profile]
 * createTable() => "profileBanner", deps: [profile]
 * createTable() => "postImage", deps: [post]
 * createTable() => "postAudio", deps: [post]
 * createTable() => "profileAudio", deps: [profile]
 * createTable() => "stripeInfo", deps: [profile]
 * createTable() => "notification", deps: [profile, profile]
 * createTable() => "subscription", deps: [profile, profile, plan]
 * createTable() => "tipPost", deps: [profile, post, profile]
 * createTable() => "profileAvatar", deps: [profile]
 *
 */

const info = {
  revision: 1,
  name: "noname",
  created: "2021-08-16T00:25:55.076Z",
  comment: "",
};

const migrationCommands = (transaction) => [
  {
    fn: "createTable",
    params: [
      "profile",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        oidcId: { type: Sequelize.STRING, field: "oidcId", unique: true },
        username: { type: Sequelize.STRING, field: "username", unique: true },
        name: { type: Sequelize.STRING, field: "name", allowNull: false },
        signupLevel: {
          type: Sequelize.INTEGER,
          field: "signupLevel",
          defaultValue: 0,
          allowNull: false,
        },
        percentageFee: {
          type: Sequelize.DOUBLE,
          field: "percentageFee",
          defaultValue: 0.15,
          allowNull: false,
        },
        isNsfw: {
          type: Sequelize.BOOLEAN,
          field: "isNsfw",
          defaultValue: false,
          allowNull: false,
        },
        aboutTiktokUsername: {
          type: Sequelize.TEXT,
          field: "aboutTiktokUsername",
          allowNull: true,
        },
        aboutInstagramUsername: {
          type: Sequelize.TEXT,
          field: "aboutInstagramUsername",
          allowNull: true,
        },
        aboutHomepageUrl: {
          type: Sequelize.TEXT,
          field: "aboutHomepageUrl",
          allowNull: true,
        },
        aboutMessage: {
          type: Sequelize.TEXT,
          field: "aboutMessage",
          allowNull: true,
        },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "category",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        slug: {
          type: Sequelize.STRING,
          field: "slug",
          unique: true,
          allowNull: false,
        },
        name: { type: Sequelize.STRING, field: "name", allowNull: false },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "genre",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        slug: {
          type: Sequelize.STRING,
          field: "slug",
          unique: true,
          allowNull: false,
        },
        name: { type: Sequelize.STRING, field: "name", allowNull: false },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        categoryId: {
          type: Sequelize.UUID,
          field: "categoryId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "category", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "post",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        title: { type: Sequelize.STRING, field: "title" },
        description: { type: Sequelize.TEXT, field: "description" },
        status: { type: Sequelize.STRING, field: "status" },
        price: { type: Sequelize.DOUBLE, field: "price" },
        currency: {
          type: Sequelize.STRING,
          field: "currency",
          defaultValue: "$",
        },
        isNsfw: {
          type: Sequelize.BOOLEAN,
          field: "isNsfw",
          defaultValue: false,
        },
        writerUsername: { type: Sequelize.STRING, field: "writerUsername" },
        customTags: {
          type: Sequelize.ARRAY(Sequelize.STRING),
          field: "customTags",
        },
        isPaid: {
          type: Sequelize.BOOLEAN,
          field: "isPaid",
          defaultValue: false,
        },
        isPublished: {
          type: Sequelize.BOOLEAN,
          field: "isPublished",
          defaultValue: false,
        },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        authorId: {
          type: Sequelize.UUID,
          field: "authorId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
        genreId: {
          type: Sequelize.UUID,
          field: "genreId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "genre", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "profileFollowProfile",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        followId: {
          type: Sequelize.UUID,
          field: "followId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "profile", key: "id" },
          unique: "profileFollowProfile_followerId_followId_unique",
        },
        followerId: {
          type: Sequelize.UUID,
          field: "followerId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "profile", key: "id" },
          unique: "profileFollowProfile_followerId_followId_unique",
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "comment",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          unique: true,
          primaryKey: true,
          allowNull: false,
        },
        message: { type: Sequelize.STRING, field: "message" },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        authorId: {
          type: Sequelize.UUID,
          field: "authorId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
        postId: {
          type: Sequelize.UUID,
          field: "postId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "post", key: "id" },
          allowNull: true,
        },
        parentId: {
          type: Sequelize.UUID,
          field: "parentId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "comment", key: "id" },
          allowNull: true,
        },
        commentId: {
          type: Sequelize.UUID,
          field: "commentId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "comment", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "profileLikeComment",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
          allowNull: false,
        },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        profileId: {
          type: Sequelize.UUID,
          field: "profileId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "profile", key: "id" },
          unique: "profileLikeComment_commentId_profileId_unique",
        },
        commentId: {
          type: Sequelize.UUID,
          field: "commentId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "comment", key: "id" },
          unique: "profileLikeComment_commentId_profileId_unique",
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "profileHasCategory",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
          allowNull: false,
        },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        profileId: {
          type: Sequelize.UUID,
          unique: "profileHasCategory_categoryId_profileId_unique",
          field: "profileId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
        categoryId: {
          type: Sequelize.UUID,
          allowNull: true,
          field: "categoryId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "category", key: "id" },
          unique: "profileHasCategory_categoryId_profileId_unique",
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "profileLikePost",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
          allowNull: false,
        },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        profileId: {
          type: Sequelize.UUID,
          field: "profileId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "profile", key: "id" },
          unique: "profileLikePost_postId_profileId_unique",
        },
        postId: {
          type: Sequelize.UUID,
          field: "postId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "post", key: "id" },
          unique: "profileLikePost_postId_profileId_unique",
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "purchasePost",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        chargeId: { type: Sequelize.STRING, field: "chargeId" },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        creatorId: {
          type: Sequelize.UUID,
          field: "creatorId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "profile", key: "id" },
          unique: "purchasePost_creatorId_postId_unique",
        },
        postId: {
          type: Sequelize.UUID,
          allowNull: true,
          field: "postId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "post", key: "id" },
          unique: "purchasePost_postId_fanId_unique",
        },
        fanId: {
          type: Sequelize.UUID,
          field: "fanId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "profile", key: "id" },
          unique: "purchasePost_postId_fanId_unique",
        },
        purchaserId: {
          type: Sequelize.UUID,
          field: "purchaserId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "plan",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          primaryKey: true,
          defaultValue: Sequelize.UUIDV4,
        },
        priceId: { type: Sequelize.STRING, field: "priceId" },
        productId: { type: Sequelize.STRING, field: "productId" },
        amountCents: { type: Sequelize.INTEGER, field: "amountCents" },
        currency: {
          type: Sequelize.STRING,
          field: "currency",
          defaultValue: "usd",
        },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        profileId: {
          type: Sequelize.UUID,
          field: "profileId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "profileBanner",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          primaryKey: true,
          defaultValue: Sequelize.UUIDV4,
        },
        mimeType: { type: Sequelize.STRING, field: "mimeType" },
        extension: { type: Sequelize.STRING, field: "extension" },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        profileId: {
          type: Sequelize.UUID,
          field: "profileId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "postImage",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          primaryKey: true,
          defaultValue: Sequelize.UUIDV4,
        },
        mimeType: { type: Sequelize.STRING, field: "mimeType" },
        extension: { type: Sequelize.STRING, field: "extension" },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        postId: {
          type: Sequelize.UUID,
          field: "postId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "post", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "postAudio",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          primaryKey: true,
          defaultValue: Sequelize.UUIDV4,
        },
        mimeType: { type: Sequelize.STRING, field: "mimeType" },
        extension: { type: Sequelize.STRING, field: "extension" },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        postId: {
          type: Sequelize.UUID,
          field: "postId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "post", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "profileAudio",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          primaryKey: true,
          defaultValue: Sequelize.UUIDV4,
        },
        mimeType: { type: Sequelize.STRING, field: "mimeType" },
        extension: { type: Sequelize.STRING, field: "extension" },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        profileId: {
          type: Sequelize.UUID,
          field: "profileId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "stripeInfo",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          primaryKey: true,
          defaultValue: Sequelize.UUIDV4,
        },
        accountId: { type: Sequelize.STRING, field: "accountId" },
        customerId: { type: Sequelize.STRING, field: "customerId" },
        detailsSubmitted: {
          type: Sequelize.BOOLEAN,
          field: "detailsSubmitted",
          defaultValue: false,
        },
        chargesEnabled: {
          type: Sequelize.BOOLEAN,
          field: "chargesEnabled",
          defaultValue: false,
        },
        payoutsEnabled: {
          type: Sequelize.BOOLEAN,
          field: "payoutsEnabled",
          defaultValue: false,
        },
        countryIso: { type: Sequelize.STRING, field: "countryIso" },
        paymentMethodId: { type: Sequelize.STRING, field: "paymentMethodId" },
        priceId: { type: Sequelize.STRING, field: "priceId" },
        productId: { type: Sequelize.STRING, field: "productId" },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        profileId: {
          type: Sequelize.UUID,
          field: "profileId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "notification",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        message: { type: Sequelize.STRING, field: "message" },
        isRead: {
          type: Sequelize.BOOLEAN,
          field: "isRead",
          defaultValue: false,
        },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        notifiedId: {
          type: Sequelize.UUID,
          allowNull: true,
          field: "notifiedId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "profile", key: "id" },
          unique: "notification_notifierId_notifiedId_unique",
        },
        notifierId: {
          type: Sequelize.UUID,
          allowNull: true,
          field: "notifierId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "profile", key: "id" },
          unique: "notification_notifierId_notifiedId_unique",
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "subscription",
      {
        id: { type: Sequelize.STRING, field: "id", primaryKey: true },
        isCancelled: { type: Sequelize.BOOLEAN, field: "isCancelled" },
        renewalDate: { type: Sequelize.DATE, field: "renewalDate" },
        stripeSubscriptionId: {
          type: Sequelize.STRING,
          field: "stripeSubscriptionId",
        },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        fanId: {
          type: Sequelize.UUID,
          field: "fanId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
        creatorId: {
          type: Sequelize.UUID,
          field: "creatorId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
        planId: {
          type: Sequelize.UUID,
          field: "planId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "plan", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "tipPost",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true,
        },
        amountCents: { type: Sequelize.INTEGER, field: "amountCents" },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        tipperId: {
          type: Sequelize.UUID,
          field: "tipperId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
        postId: {
          type: Sequelize.UUID,
          allowNull: true,
          field: "postId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "post", key: "id" },
          unique: "tipPost_postId_profileId_unique",
        },
        profileId: {
          type: Sequelize.UUID,
          field: "profileId",
          onUpdate: "CASCADE",
          onDelete: "CASCADE",
          references: { model: "profile", key: "id" },
          unique: "tipPost_postId_profileId_unique",
        },
      },
      { transaction },
    ],
  },
  {
    fn: "createTable",
    params: [
      "profileAvatar",
      {
        id: {
          type: Sequelize.UUID,
          field: "id",
          primaryKey: true,
          defaultValue: Sequelize.UUIDV4,
        },
        mimeType: { type: Sequelize.STRING, field: "mimeType" },
        extension: { type: Sequelize.STRING, field: "extension" },
        createdAt: {
          type: Sequelize.DATE,
          field: "createdAt",
          allowNull: false,
        },
        updatedAt: {
          type: Sequelize.DATE,
          field: "updatedAt",
          allowNull: false,
        },
        profileId: {
          type: Sequelize.UUID,
          field: "profileId",
          onUpdate: "CASCADE",
          onDelete: "SET NULL",
          references: { model: "profile", key: "id" },
          allowNull: true,
        },
      },
      { transaction },
    ],
  },
];

const rollbackCommands = (transaction) => [
  {
    fn: "dropTable",
    params: ["profile", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["post", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["comment", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["profileFollowProfile", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["profileLikePost", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["profileLikeComment", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["category", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["profileHasCategory", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["genre", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["purchasePost", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["subscription", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["profileBanner", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["postImage", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["postAudio", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["profileAudio", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["stripeInfo", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["notification", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["plan", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["tipPost", { transaction }],
  },
  {
    fn: "dropTable",
    params: ["profileAvatar", { transaction }],
  },
];

const pos = 0;
const useTransaction = true;

const execute = (queryInterface, sequelize, _commands) => {
  let index = pos;
  const run = (transaction) => {
    const commands = _commands(transaction);
    return new Promise((resolve, reject) => {
      const next = () => {
        if (index < commands.length) {
          const command = commands[index];
          console.log(`[#${index}] execute: ${command.fn}`);
          index++;
          queryInterface[command.fn](...command.params).then(next, reject);
        } else resolve();
      };
      next();
    });
  };
  if (useTransaction) return queryInterface.sequelize.transaction(run);
  return run(null);
};

module.exports = {
  pos,
  useTransaction,
  up: (queryInterface, sequelize) =>
    execute(queryInterface, sequelize, migrationCommands),
  down: (queryInterface, sequelize) =>
    execute(queryInterface, sequelize, rollbackCommands),
  info,
};
