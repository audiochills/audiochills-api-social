const app = require('../src/app').app;
const env = process.env.NODE_ENV || 'development';

module.exports = {
  [env]: app.get('sequelize')
};

