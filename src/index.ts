import logger from './logger';
import http from 'http';
import httpShutdown from 'http-shutdown';
import { promisify } from 'es6-promisify';
import pEvent from 'p-event';
import 'source-map-support/register';
import { Application } from './declarations';

async function start(termination: Promise<any>) {
  logger.info('Feathers application starting...');

  // Asynchronously import ./app to catch every synchronous exception as a promise-rejection
  const { app } = await Promise.race([termination, import('./app')]) as { app: Application };
  Object.assign(global, { app });

  // Applies a server.shutdown() function for gracefully shutdown nodejs's http.Server
  const server = httpShutdown(http.createServer(app));
  await Promise.race([termination, app.setup(server)]);

  // Wait for sequelize initialization to be done
  await Promise.race([termination, app.get('sequelizeSync')]);

  // Start http.Server and wait using a promise
  const host = app.get('host');
  const port = app.get('port');

  await Promise.race([termination, promisify(cb => server.listen(port, host, () => cb(undefined)))()]);

  try {
    logger.info('Feathers application started on http://%s:%d', host, port);

    // Wait for termination promise to stop the application
    await termination;

  } finally {
    logger.info('Feathers application stopping...');

    // In any cases, gracfully shutdown http.Server and wait
    await promisify(cb => server.shutdown(cb))();
    logger.info('Feathers application stopped');
  }
}

async function main() {

  try {
    // Create a promise from termination events on process.on(<event>)
    const termination = pEvent(process, [], { rejectionEvents: ['SIGINT', 'SIGTERM', 'unhandledRejection'] });

    // Run start queue and pass termination promise
    await start(termination);

    // Let pending errors to print and exit
    process.nextTick(() => process.exit(0));

  } catch (e) {
    // Print error to logger
    logger.error(e);
    if (e.error) logger.error(e.error.stack || e.error);
    if (e.errors && e.errors instanceof Array) e.errors.forEach((e2: any) => logger.error(e2.stack || e2));

    // Convert error to uncaughtException event for source-map-support plugin
    process.emit('uncaughtException', e);

    // Let pending errors to print and exit
    process.nextTick(() => process.exit(1));
  }
}

main();
