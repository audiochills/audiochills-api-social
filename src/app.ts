import path from 'path';
import favicon from 'serve-favicon';
import compress from 'compression';
import helmet from 'helmet';
import feathers from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio';
import configuration from '@feathersjs/configuration';
import express from '@feathersjs/express';
import logger from './logger';
import middleware from './middleware';
import services from './services';
import appHooks from './app.hooks';
import channels from './channels';
import sequelize from './sequelize';
import { Application, ServiceTypes } from './declarations';
import aws from './aws';
import authentication from './authentication';
import amplitude from './amplitude';
import swagger from 'feathers-swagger';
// import cors from 'cors';

export const app: Application = express(feathers<ServiceTypes>());

// Load app configuration
app.configure(configuration());
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet(app.get('helmet')));
// app.use(cors(app.get('cors')));

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', '*');
  next();
});
app.use(compress());
app.configure(socketio());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));

app.configure(swagger(app.get('swagger')));
// app.use('/', express.static(app.get('public')));

// Set up Plugins and providers
app.configure(express.rest());

app.configure(authentication);
app.configure(sequelize);

// Configure other middleware (see `middleware/index.ts`)
app.configure(middleware);
// Set up our services (see `services/index.ts`)
app.configure(services);
// Set up event channels (see channels.ts)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger } as any));

app.hooks(appHooks);

app.configure(aws);

app.configure(amplitude);

export default app;
