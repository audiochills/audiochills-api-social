import { authenticate } from '@feathersjs/authentication';
import { iff, fastJoin, isProvider, disallow } from 'feathers-hooks-common';
import { setField } from 'feathers-authentication-hooks';


const includeRelations = fastJoin({
  joins: {
    notifier: () => async (notification, { app }) => notification.notifier = await app.service('profile').get(notification.notifierId),
    notified: () => async (notification, { app }) => notification.notified = await app.service('profile').get(notification.notifiedId),
  }
});

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.notifiedId', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [filterAuthorized],
    get: [filterAuthorized],
    create: [disallow('external')],
    update: [disallow('external')],
    patch: [disallow('external')],
    remove: [disallow('external')],
  },

  after: {
    all: [],
    find: [includeRelations],
    get: [includeRelations],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
