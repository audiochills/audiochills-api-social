// Initializes the `follow` service on path `/follow`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ProfileFollowProfile } from './profile-follow-profile.class';
import createModel from '../../models/profile-follow-profile';
import hooks from './profile-follow-profile.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'profile-follow-profile': ProfileFollowProfile & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/profile-follow-profile', new ProfileFollowProfile(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('profile-follow-profile');

  service.hooks(hooks);
}
