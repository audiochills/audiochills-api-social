import { authenticate } from '@feathersjs/authentication/lib/hooks';
import { disallow, iff, isProvider } from 'feathers-hooks-common';
import { setField } from 'feathers-authentication-hooks';

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.followerId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.followerId', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [filterAuthorized],
    get: [filterAuthorized],
    create: [filterAuthorized, associateAuthorized],
    update: [filterAuthorized, associateAuthorized, disallow('external')],
    patch: [filterAuthorized, associateAuthorized, disallow('external')],
    remove: [filterAuthorized, associateAuthorized],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
