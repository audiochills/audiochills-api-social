import { authenticate } from '@feathersjs/authentication/lib/hooks';
import { alterItems, disallow, iff, isProvider } from 'feathers-hooks-common';
import { hooks as sequelizeHooks } from 'feathers-sequelize';
import { setField } from 'feathers-authentication-hooks';


const constructBucketPath = (profileId: string, extension: string): string => (`profile/${profileId}/image/banner/${profileId}.${extension}`);

const attachS3Urls = alterItems(async (record, context) => {
  const params = {
    Bucket: context.app.get('aws')['s3Presign']['bucket'],
    Key: constructBucketPath(record.profileId, record.extension),
    Expires: 60 * 60 * 60,
    ContentType: record.mimeType
  };

  record.setDataValue('uploadUrl', context.app.get('clientS3').getSignedUrl('putObject', params));
  delete params.ContentType;
  record.setDataValue('downloadUrl', context.app.get('clientS3').getSignedUrl('getObject', params));

  delete params.Expires;

  try{
    await (context.app.get('clientS3').headObject(params).promise());
    record.setDataValue('exists', true);
  }
  catch(e){
    record.setDataValue('exists', false);
  }

});

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.profileId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.profileId', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [filterAuthorized, associateAuthorized, disallow('external')],
    update: [filterAuthorized, associateAuthorized],
    patch: [filterAuthorized, associateAuthorized],
    remove: [filterAuthorized, associateAuthorized, disallow('external')],
  },

  after: {
    all: [sequelizeHooks.hydrate(), attachS3Urls , sequelizeHooks.dehydrate()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
