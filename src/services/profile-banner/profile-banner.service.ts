// Initializes the `profileBanner` service on path `/profile-banner`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ProfileBanner } from './profile-banner.class';
import createModel from '../../models/profile-banner.model';
import hooks from './profile-banner.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'profile-banner': ProfileBanner & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/profile-banner', new ProfileBanner(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('profile-banner');

  service.hooks(hooks);
}
