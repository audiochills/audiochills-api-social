// import { Hook } from '@feathersjs/feathers';
import { alterItems } from 'feathers-hooks-common';
// import { hooks as sequelizeHooks } from 'feathers-sequelize';
import { uuid } from 'uuidv4';
// import processNestedSearch from '../../hooks/process-nested-search';


const constructBucketPath = (postId: string, extension: string): string => (`posts/${postId}/image/${postId}.${extension}`);

const createImage = alterItems((record) => record.id = uuid());


const presignWrite = alterItems((record, context) => {

  const params = {
    Bucket: context.app.get('aws')['s3Presign']['bucket'],
    Key: constructBucketPath(record.postId, record.extension),
    Expires: 60 * 60 * 24 * 30,
    ContentType: record.mimeType
  };

  //execute

  const url = context.app.get('clientS3').getSignedUrl('putObject', params);
  record.uploadUrl = url;


});


const presignRead = alterItems(async (record, context)  => {

  //plan
  const params = {
    Bucket: context.app.get('aws')['s3Presign']['bucket'],
    Key: constructBucketPath(record.postId, record.extension),
    Expires: 60 * 60 * 24 * 30,
    // ContentType: record.mimeType
  };


  const headParams = params;
  delete headParams.Expires;

  const url = context.app.get('clientS3').getSignedUrl('getObject', params);
  record.downloadUrl = url;

  try{
    await (context.app.get('clientS3').headObject(headParams).promise());
    record.exists =  true;
  }
  catch(e){
    record.exists =  false;
  }

});



export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [createImage ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [ presignRead ],
    get: [  presignRead ],
    create: [ presignWrite  ],
    update: [ presignWrite  ],
    patch: [ presignWrite  ],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
