// Initializes the `postImage` service on path `/post-image`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PostImage } from './post-image.class';
import createModel from '../../models/post-image.model';
import hooks from './post-image.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'post-image': PostImage & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/post-image', new PostImage(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('post-image');

  service.hooks(hooks);
}
