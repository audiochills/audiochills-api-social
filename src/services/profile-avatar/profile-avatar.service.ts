// Initializes the `profileAvatar` service on path `/profile-avatar`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ProfileAvatar } from './profile-avatar.class';
import createModel from '../../models/profile-avatar.model';
import hooks from './profile-avatar.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'profile-avatar': ProfileAvatar & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/profile-avatar', new ProfileAvatar(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('profile-avatar');

  service.hooks(hooks);
}
