import axios from 'axios';
import { alterItems } from 'feathers-hooks-common';
import logger from '../../logger';
import { authenticate } from '@feathersjs/authentication/lib/hooks';
import { iff, isProvider } from 'feathers-hooks-common';
import { setField } from 'feathers-authentication-hooks';


const updatePlan = alterItems(async (record, context) =>{

  if(!context.data.amountCents){return;}

  const planInfo = await context.app.services['plan'].get(context.id);

  logger.debug('plan Info: ', planInfo);

  if(planInfo['priceId'] && planInfo['productId']){
    //update
    const newPlanId = await axios({
      method: 'POST',
      url: context.app.get('stripe')['endpoint']+'/creator/update_plan',
      data: {
        stripe_product_id: planInfo.productId,
        stripe_price_id: planInfo.priceId,
        amount_cents: planInfo.amountCents,
        creator_id: context.params.profile.id,
      }
    });

    logger.debug('updated plan: ', newPlanId.data);
    record.priceId = newPlanId.data.id;
    record.productId = newPlanId.data.product;

  }else{
    //create
    const newPlanId = await axios({
      method: 'POST',
      url: context.app.get('stripe')['endpoint']+'/creator/register_plan',
      data: {
        amount_cents: context.data.amountCents,
        creator_id: context.params.profile.id,
        creator_username: context.params.profile.username
      }
    });
    logger.debug('created plan: ', newPlanId.data);
    record.priceId = newPlanId.data.id;
    record.productId = newPlanId.data.product;
  }

});

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.profileId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.profileId', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [filterAuthorized, associateAuthorized],
    update: [filterAuthorized, associateAuthorized],
    patch: [filterAuthorized, associateAuthorized],
    remove: [filterAuthorized, associateAuthorized]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
