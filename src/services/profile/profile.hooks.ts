import { hooks as sequelizeHooks } from 'feathers-sequelize';
import { alterItems, disallow, iff, isProvider } from 'feathers-hooks-common';
import { authenticate } from '@feathersjs/authentication/lib/hooks';
import { setField } from 'feathers-authentication-hooks';
import { eventNames } from '../../amplitude';

const includeCount = alterItems(async (record, context) => {

  async function joinOneToOneDataValue(name: string, serviceName: string) {
    const query = { profileId: record.id };
    const service = context.app.service(serviceName);
    const found = await service.find({ paginate: false, query });
    const foundOrCreated = found.find(Boolean) || await service.create(query);
    record.setDataValue(name, foundOrCreated);
  }


  record.setDataValue('followerCount', await record.countFollower());
  record.setDataValue('categories', await record.getCategories());
  await Promise.all([
    Promise.resolve().then(async () => record.setDataValue('followerCount', await record.countFollower())),
    Promise.resolve().then(async () => record.setDataValue('categories', await record.getCategories())),
    joinOneToOneDataValue('profileAudio', 'profile-audio'),
    joinOneToOneDataValue('plan', 'plan'),
    joinOneToOneDataValue('stripeInfo', 'stripe-info'),
    joinOneToOneDataValue('profileAvatar', 'profile-avatar'),
    joinOneToOneDataValue('profileBanner', 'profile-banner'),
  ]);
  // record.setDataValue('profileAudio', (await context.app.service('profile-audio').find({ paginate: false, query: { profileId: record.id }})).find(Boolean) || await context.app.service('profile-audio').create({ profileId: record.id }));
  // record.setDataValue('plan', (await context.app.service('plan').find({ paginate: false, query: { profileId: record.id }})).find(Boolean) || await context.app.service('plan').create({ profileId: record.id }));
  // record.setDataValue('stripeInfo', (await context.app.service('stripe-info').find({ paginate: false, query: { profileId: record.id }})).find(Boolean) || await context.app.service('stripe-info').create({ profileId: record.id }));
  // record.setDataValue('profileImage', (await context.app.service('profile-image').find({ paginate: false, query: { profileId: record.id }})).find(Boolean) || await context.app.service('profile-image').create({ profileId: record.id }));
  // record.setDataValue('profileBanner', (await context.app.service('profile-banner').find({ paginate: false, query: { profileId: record.id }})).find(Boolean) || await context.app.service('profile-banner').create({ profileId: record.id }));
});

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.id', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.id', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [filterAuthorized, associateAuthorized, disallow('external')],
    update: [filterAuthorized, associateAuthorized],
    patch: [filterAuthorized, associateAuthorized],
    remove: [filterAuthorized, associateAuthorized, disallow('external')],
  },

  after: {
    all: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
