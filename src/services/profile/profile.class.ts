import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { ProfileAttributes } from '../../attributes';
import { Application } from '../../declarations';

export class Profile extends Service<ProfileAttributes> {
  app: Application;
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<SequelizeServiceOptions>, app: Application) {
    super(options);
    this.app = app;
  }
}
