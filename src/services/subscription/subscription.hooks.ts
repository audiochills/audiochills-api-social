import { authenticate } from '@feathersjs/authentication/lib/hooks';
import axios from 'axios';
import { setField } from 'feathers-authentication-hooks';
import { alterItems, disallow, iff, isProvider } from 'feathers-hooks-common';
import { uuid } from 'uuidv4';
import { eventNames } from '../../amplitude';
import logger from '../../logger';

const isDuplicate = alterItems(async (record, context) =>{

  const subscriptionDB = await context.app.services['subscription'].find({
    query:{
      creatorId: context.data.creatorId,
      fanId: context.params.profile.id,
      $sort: {
        createdAt: -1
      }
    }
  });

  logger.debug('does subscription exist: ', subscriptionDB);


  if(subscriptionDB.total > 0 ){

    if(subscriptionDB.data[0].isCancelled === null || subscriptionDB.data[0].renewalDate === null){
      logger.debug('subscription doesn"t exist' );
      return;
    }

    //subscription isn't cancelled, it's still running and renewal succeeds (renewal date > current date)
    if(subscriptionDB.data[0].isCancelled === false && subscriptionDB.data[0].renewalDate > new Date()){
      throw new Error('Subscription active');
    }

    //subscription is cancelled but the due data isn't finished
    if(subscriptionDB.data[0].isCancelled && subscriptionDB.data[0].renewalDate > new Date()){
      throw new Error('Subscription is cancelled but the due data isn\'t finished');
    }
    //subscription is cancelled and expired
    else{
      logger.debug('subscription is cancelled and expired');
    }

  }else{
    logger.debug('subscription doesn"t exist' );

  }


});


const initSubscription = alterItems(async (record) =>record.id = uuid());


const createSubscription = alterItems(async (record, context) =>{

  // creatorId
  // fanId
  const fanProfile = await context.app.services['profile'].get(context.params.profile.id);
  const creatorProfile = await context.app.services['profile'].get(context.data.creatorId);


  //new subscription, create it
  const subscription = await axios({
    method: 'POST',
    url: context.app.get('stripe')['endpoint']+'/customer/subscription',
    data: {
      db_sub_id: record.id,
      customer_id: fanProfile.stripeInfo.customerId,
      fan_id: fanProfile.id,
      account_id: creatorProfile.stripeInfo.accountId,
      creator_id: creatorProfile.stripeInfo.profileId,
      creator_username: creatorProfile.username,
      price_id: creatorProfile.plan.priceId,
      plan_id: creatorProfile.plan.id
    }
  });
  logger.debug('subscription response: ', subscription.data);

  record.url = subscription.data;


});




// const cancelSubscription = alterItems(async (record, context) =>{

//   // cancel

//   if(!context.data.cancel){return;}

//   //cancel subscription
//   await axios({
//     method: 'POST',
//     url: context.app.get('stripe')['endpoint']+'/customer/cancel_subscription',
//     data: {
//       subscription_id: context.id,
//     }
//   });

//   record.isCancelled = true;

// });

const increaseTime = alterItems(async (record, context) =>{

  if(!context.data.paid){return;}

  delete context.data.paid;

  record.renewalDate = (new Date()).setDate((new Date()).getDate() + 30);
  record.isCancelled = false;

  const creatorProfile = await context.app.service('profile').get(context.data.creator_id);
  context.data.planId = creatorProfile.plan.id;

  delete context.data.creator_id;

  context.app.get('amplitudeClient').logEvent({
    event_type: eventNames.SUBSCRIPTION_SUCCESSFUL,
    user_id: creatorProfile.id,
    event_properties: {
      creatorUsername: creatorProfile.username,
      creatorCategories: creatorProfile.categories,
      priceValue: (creatorProfile.plan?.amountCents ?? -1)/100
    }
  });

});

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.fanId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.fanId', allowUndefined: true }));


export default {
  before: {
    all: [],
    find: [filterAuthorized],
    get: [filterAuthorized],
    create: [filterAuthorized, associateAuthorized, isDuplicate, initSubscription],
    update: [filterAuthorized, associateAuthorized],
    patch: [filterAuthorized, associateAuthorized, increaseTime],
    remove: [filterAuthorized, associateAuthorized]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
