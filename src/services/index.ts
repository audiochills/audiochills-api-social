import { Application } from '../declarations';
import profile from './profile/profile.service';
import post from './post/post.service';
import comment from './comment/comment.service';
import profileFollowProfile from './profile-follow-profile/profile-follow-profile.service';
import profileLikePost from './profile-like-post/profile-like-post.service';
import profileLikeComment from './profile-like-comment/profile-like-comment.service';
import category from './category/category.service';
import profileHasCategory from './profile-has-category/profile-has-category.service';
import genre from './genre/genre.service';
import purchasePost from './purchase-post/purchase-post.service';
import subscription from './subscription/subscription.service';
import profileBanner from './profile-banner/profile-banner.service';
import postImage from './post-image/post-image.service';
import postAudio from './post-audio/post-audio.service';
import profileAudio from './profile-audio/profile-audio.service';
import stripeInfo from './stripe-info/stripe-info.service';
import notification from './notification/notification.service';
import plan from './plan/plan.service';
import tipPost from './tip-post/tip-post.service';
import profileAvatar from './profile-avatar/profile-avatar.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(profile);
  app.configure(post);
  app.configure(comment);
  app.configure(profileFollowProfile);
  app.configure(profileLikePost);
  app.configure(profileLikeComment);
  app.configure(category);
  app.configure(profileHasCategory);
  app.configure(genre);
  app.configure(purchasePost);
  app.configure(subscription);
  app.configure(profileBanner);
  app.configure(postImage);
  app.configure(postAudio);
  app.configure(profileAudio);
  app.configure(stripeInfo);
  app.configure(notification);
  app.configure(plan);
  app.configure(tipPost);
  app.configure(profileAvatar);
}
