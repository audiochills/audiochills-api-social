// Initializes the `stripeInfo` service on path `/stripe-info`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { StripeInfo } from './stripe-info.class';
import createModel from '../../models/stripe-info.model';
import hooks from './stripe-info.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'stripe-info': StripeInfo & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/stripe-info', new StripeInfo(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('stripe-info');

  service.hooks(hooks);
}
