import axios from 'axios';
import { alterItems,  } from 'feathers-hooks-common';
import { eventNames } from '../../amplitude';
import logger from '../../logger';
import { authenticate } from '@feathersjs/authentication/lib/hooks';
import { disallow, iff, isProvider } from 'feathers-hooks-common';
import { setField } from 'feathers-authentication-hooks';

const createCustomer = alterItems(async (record, context) => {
  const response = await axios({
    method: 'GET',
    url: context.app.get('stripe')['endpoint']+'/customer/register_customer',
    params: {profile_id: record.profileId}
  });

  record.customerId = response.data['customerId'];
});


const createStripeAccountId = alterItems(async (record, context) =>{
  if (!context.data.createAccount) return;
  const submitDetailsUrl = await axios({
    method: 'POST',
    url: context.app.get('stripe')['endpoint']+'/creator/register_account',
    data: {
      profile_id: context.params.profile.id,
      country_iso: context.data.countryIso
    }
  });
  record.accountId = submitDetailsUrl.data.id;
});


const onboardingUrls = alterItems(async (record, context) => {

  if (!record.accountId) return;

  if (record.detailsSubmitted && record.chargesEnabled) {

    const verifyAccountUrl = await axios({
      method: 'GET',
      url: context.app.get('stripe')['endpoint']+'/creator/verify_account',
      params: {
        acct_id: record.accountId,
      }
    });
    record.url = verifyAccountUrl.data.url;
    logger.debug('stripe verify_account', verifyAccountUrl.data);
  } else {
    const submitDetailsUrl = await axios({
      method: 'GET',
      url: context.app.get('stripe')['endpoint']+'/creator/link_account',
      params: {
        acct_id: record.accountId,
        refresh_url: 'https://www.audiochills.art/become-creator',
        return_url: 'https://www.audiochills.art/become-creator'
      }
    });
    record.url = submitDetailsUrl.data.url;
    logger.debug('stripe link_account', submitDetailsUrl.data);

  }
});


const customerPortal = alterItems( async (record, context) => {

  const urlObj = await axios({
    method: 'POST',
    url: context.app.get('stripe')['endpoint']+'/customer/manage-subscription',
    data: {
      customer_id: record.customerId,
      return_url: 'https://www.audiochills.art'
    }
  });
  record.customerPortal = urlObj.data;

});

// const logCreatorStatus = alterItems(async (record, context) => {
//   if(!context.data.shouldLog){return;}

//   const profile = await context.app.service('profile').get(context.id);

//   const {chargesEnabled, detailsSubmitted, payoutsEnabled} = profile.stripeInfo;

//   if(chargesEnabled && detailsSubmitted && payoutsEnabled) {} //nothing to log
//   else if(context.data.chargesEnabled && context.data.detailsSubmitted && context.data.payoutsEnabled) {
//     context.app.get('amplitudeClient').logEvent({
//       event_type: eventNames.BECOME_CREATOR,
//       user_id: profile.id,
//       event_properties: {
//         creatorUsername: profile.username,
//         userAudience: profile.followerCount,
//         detailsSubmitted: true,
//         chargesEnabled: true,
//         payoutsEnabled: true
//       }
//     });

//   }

// });

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.profileId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.profileId', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [filterAuthorized, associateAuthorized, disallow('external')],
    update: [filterAuthorized, associateAuthorized],
    patch: [filterAuthorized, associateAuthorized],
    remove: [filterAuthorized, associateAuthorized, disallow('external')]
  },

  after: {
    all: [],
    find: [ ],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};


// const updateCardDetails = alterItems(async (record, context) =>{

//   if(!context.data.addCard){return;}


//   const profileStripeInfo = await context.app.services['stripe-info'].get(context.params.profile.id);


//   const submitDetailsUrl = await axios({
//     method: 'POST',
//     url: context.app.get('stripe')['endpoint']+'/customer/add_card',
//     data: {
//       customer_id: profileStripeInfo.customerId,
//       card_number: context.data.cardNumber,
//       card_exp_month: context.data.expMonth,
//       card_exp_year: context.data.expYear,
//       card_cvc: context.data.cvc,
//     }
//   });
//   logger.debug('submit details! ', submitDetailsUrl);
//   record.paymentMethodId = submitDetailsUrl.data.default_source;

// });
