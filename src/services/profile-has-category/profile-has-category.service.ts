// Initializes the `profile-has-category` service on path `/profile-has-category`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ProfileHasCategory } from './profile-has-category.class';
import createModel from '../../models/profile-has-category.model';
import hooks from './profile-has-category.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'profile-has-category': ProfileHasCategory & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate'),
  };

  // Initialize our service with any options it requires
  app.use('/profile-has-category', new ProfileHasCategory(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('profile-has-category');

  service.hooks(hooks);
}
