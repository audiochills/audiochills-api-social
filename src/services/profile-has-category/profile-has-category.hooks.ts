import { hooks as sequelizeHooks } from 'feathers-sequelize';
import { alterItems } from 'feathers-hooks-common';
import { authenticate } from '@feathersjs/authentication/lib/hooks';
import { disallow, iff, isProvider } from 'feathers-hooks-common';
import { setField } from 'feathers-authentication-hooks';

const includeCount = alterItems(async (record) => {
  record.setDataValue('category', await record.getCategory());
  record.setDataValue('profile', await record.getProfile());
});

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.profileId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.profileId', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [filterAuthorized, associateAuthorized],
    update: [filterAuthorized, associateAuthorized, disallow('external')],
    patch: [filterAuthorized, associateAuthorized, disallow('external')],
    remove: [filterAuthorized, associateAuthorized],
  },

  after: {
    all: [],
    find: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    get: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    create: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    update: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    patch: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
