// Initializes the `ProfileLikePost` service on path `/profile-like-post`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ProfileLikePost } from './profile-like-post.class';
import createModel from '../../models/profile-like-post.model';
import hooks from './profile-like-post.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'profile-like-post': ProfileLikePost & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/profile-like-post', new ProfileLikePost(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('profile-like-post');

  service.hooks(hooks);
}
