import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { GenreAttributes } from '../../attributes';
import { Application } from '../../declarations';

export class Genre extends Service<GenreAttributes> {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<SequelizeServiceOptions>, app: Application) {
    super(options);
  }
}
