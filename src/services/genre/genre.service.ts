// Initializes the `genre` service on path `/genre`
import { Application } from '../../declarations';
import { Genre } from './genre.class';
import createModel from '../../models/genre.model';
import hooks from './genre.hooks';
import { ServiceAddons } from '@feathersjs/feathers';
import { GenreAttributes } from '../../attributes';

declare module '../../declarations' {
  interface ServiceTypes {
    'genre': Genre & ServiceAddons<GenreAttributes>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
  };

  // Initialize our service with any options it requires
  app.use('/genre', new Genre(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('genre');

  service.hooks(hooks);
}
