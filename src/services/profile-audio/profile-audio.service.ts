// Initializes the `profileAudio` service on path `/profile-audio`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ProfileAudio } from './profile-audio.class';
import createModel from '../../models/profile-audio.model';
import hooks from './profile-audio.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'profile-audio': ProfileAudio & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/profile-audio', new ProfileAudio(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('profile-audio');

  service.hooks(hooks);
}
