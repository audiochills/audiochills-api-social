import { hooks as sequelizeHooks } from 'feathers-sequelize';
import { alterItems, disallow } from 'feathers-hooks-common';

const includeProfiles = alterItems(async (record) => {
  record.setDataValue('profiles', await record.getProfiles());
});
const includeGenres = alterItems(async (record) => {
  record.setDataValue('genres', await record.getGenres());
});

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [disallow('external')],
    update: [disallow('external')],
    patch: [disallow('external')],
    remove: [disallow('external')]
  },

  after: {
    all: [],
    find: [sequelizeHooks.hydrate(), includeProfiles, includeGenres, sequelizeHooks.dehydrate()],
    get: [sequelizeHooks.hydrate(), includeProfiles, includeGenres, sequelizeHooks.dehydrate()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
