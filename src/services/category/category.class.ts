import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { CategoryAttributes } from '../../attributes';
import { Application } from '../../declarations';

export class Category extends Service<CategoryAttributes> {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<SequelizeServiceOptions>, app: Application) {
    super(options);
  }
}
