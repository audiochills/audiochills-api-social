import { authenticate } from '@feathersjs/authentication/lib/hooks';
import axios from 'axios';
import { setField } from 'feathers-authentication-hooks';
import { alterItems, iff, isProvider } from 'feathers-hooks-common';
import { eventNames } from '../../amplitude';


const tipPost = alterItems( async (record, context)=>{

  const fanId = context.params.profile.id;

  const fanProfile = context.params.profile;
  const post = await context.app.services['post'].get(context.data.postId);
  const creatorProfile = await context.app.services['profile'].get(post.authorId);

  const response = await axios({
    method: 'POST',
    url: context.app.get('stripe')['endpoint']+'/customer/unlock_or_tip',
    data: {
      amount_cents: context.data?.amountCents, // fan.customerId,
      account_id: creatorProfile.stripeInfo.accountId,
      customer_id: fanProfile.stripeInfo.customerId,
      creator_id: post.authorId,
      fan_id: context.params.profile.id,
      is_post_unlock: false,
      item_name: 'Tip: '+ post.title,
      post_id: post.id
    }
  });

  //log amplitude

  context.app.get('amplitudeClient').logEvent({
    event_type: eventNames.SEND_TIP,
    user_id: fanId,
    event_properties: {
      userAudience: fanProfile.followerCount,
      creatorUsername: creatorProfile.username,
      creatorAudience: creatorProfile.followerCount,
      tipValue: (context.data?.amountCents ?? -1)/100
    }
  });

  context.result = {url: response.data};
});

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.tipperId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.tipperId', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [filterAuthorized],
    get: [filterAuthorized],
    create: [filterAuthorized, associateAuthorized],
    update: [filterAuthorized, associateAuthorized],
    patch: [filterAuthorized, associateAuthorized],
    remove: [filterAuthorized, associateAuthorized]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
