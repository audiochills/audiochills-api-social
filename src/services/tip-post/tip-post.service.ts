// Initializes the `tipPost` service on path `/tip-post`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { TipPost } from './tip-post.class';
import createModel from '../../models/tip-post.model';
import hooks from './tip-post.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'tip-post': TipPost & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/tip-post', new TipPost(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('tip-post');

  service.hooks(hooks);
}
