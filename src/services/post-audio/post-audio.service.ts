// Initializes the `postAudio` service on path `/post-audio`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PostAudio } from './post-audio.class';
import createModel from '../../models/post-audio.model';
import hooks from './post-audio.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'post-audio': PostAudio & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/post-audio', new PostAudio(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('post-audio');

  service.hooks(hooks);
}
