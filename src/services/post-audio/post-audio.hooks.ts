import { HookContext } from '@feathersjs/feathers';
import { hooks as sequelizeHooks } from 'feathers-sequelize';
import { alterItems } from 'feathers-hooks-common';
import { uuid } from 'uuidv4';
import app from '../../app';


//plan

const constructPostBucketPath = (postId: string, extension: string): string => (`posts/${postId}/audio/${postId}.${extension}`);


const createAudio = alterItems((record) => record.id = uuid());


const presignWrite = alterItems((record, context) => {

  const params = {
    Bucket: context.app.get('aws')['s3Presign']['bucket'],
    Key: constructPostBucketPath(record.postId, record.extension),
    Expires: 60 * 60 * 60,
    ContentType: record.mimeType
  };
  //execute
  const url = context.app.get('clientS3').getSignedUrl('putObject', params);
  record.uploadUrl = url;
  record.params = params;
});


const presignRead = alterItems( async (record, context)  => {

  if(!record.dataValues.isAuthorized){return;}
  //plan
  const params = {
    Bucket: context.app.get('aws')['s3Presign']['bucket'],
    Key: constructPostBucketPath(record.dataValues.postId, record.dataValues.extension),
    Expires: 60 * 60 * 60
  };

  const headParams = params;
  delete headParams.Expires;

  const url = context.app.get('clientS3').getSignedUrl('getObject', params);
  record.setDataValue('downloadUrl', url);

  try{
    await (context.app.get('clientS3').headObject(headParams).promise());
    record.setDataValue('exists', true);
  }
  catch(e){
    record.setDataValue('exists', false);
  }

});


// eslint-disable-next-line @typescript-eslint/no-unused-vars
const authorizeReadAudio =  alterItems( async (record, context)  => {



  if(!record.getDataValue('post').getDataValue('isPaid')){ // profile audio is always free
    record.setDataValue('isAuthorized', true);
    return;
  }

  const fanId = context.params?.profile?.id;
  const post = record.getDataValue('post');


  //check if user is subbed
  const [{total: totalPurchased}, {total: totalSub, data: subsData}] = await Promise.all([
    app.services['purchase-post'].find({query:{postId: post.getDataValue('id'), fanId}}) as any,
    app.services['subscription'].find({
      query: {
        creatorId: post.getDataValue('authorId'),
        fanId: fanId,
        $sort: {
          createdAt: -1
        }},

    }) as any,
  ]);

  const isAuthorized =
    fanId === post.getDataValue('authorId') ||
    totalPurchased > 0 ||
    totalSub > 0 && subsData[0].renewalDate > new Date();


  //final decision
  record.setDataValue('isAuthorized', isAuthorized);


});


// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const includePost = (context: HookContext) => {
  context.params.sequelize = {
    raw: false,
    include: [
      {
        model: context.app.services.post.Model,
        raw: false
      },
    ],
  };
  return context;
};


export default {
  before: {
    all: [],
    find: [ includePost ],
    get: [ includePost ], // paramsFromClient('fanId') in the case of user offline?
    create: [ createAudio ],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [ sequelizeHooks.hydrate(), authorizeReadAudio, presignRead , sequelizeHooks.dehydrate() ], //[readPresignArray as any as Hook],
    get:[ sequelizeHooks.hydrate(), authorizeReadAudio, presignRead , sequelizeHooks.dehydrate() ], //
    create: [ presignWrite ],
    update: [ presignWrite ],
    patch: [ presignWrite ],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
