// Initializes the `purchasePost` service on path `/purchase-post`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { PurchasePost } from './purchase-post.class';
import createModel from '../../models/purchase-post.model';
import hooks from './purchase-post.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'purchase-post': PurchasePost & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/purchase-post', new PurchasePost(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('purchase-post');

  service.hooks(hooks);
}
