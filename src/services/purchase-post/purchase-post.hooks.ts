import { authenticate } from '@feathersjs/authentication/lib/hooks';
import axios from 'axios';
import { setField } from 'feathers-authentication-hooks';
import { alterItems, iff, isProvider } from 'feathers-hooks-common';
import { eventNames } from '../../amplitude';
import logger from '../../logger';



const purchasePost = alterItems( async (record, context)=>{

  //fanId
  //postId



  logger.debug(context.data);

  const fanProfile = context.params.profile;
  const post = await context.app.services['post'].get(context.data.postId);
  const creatorProfile = await context.app.services['profile'].get(post.authorId);
  const postPurchase = await context.app.services['purchase-post'].find({query: {postId: post.id, fanId: fanProfile.id }});

  if(postPurchase.total > 0){ throw new Error('Post already purchased');}

  const response = await axios({
    method: 'POST',
    url: context.app.get('stripe')['endpoint']+'/customer/unlock_or_tip',
    data: {
      amount_cents: post.price, // fan.customerId,
      account_id: creatorProfile.stripeInfo.accountId,
      customer_id: fanProfile.stripeInfo.customerId,
      creator_id: post.authorId,
      fan_id: fanProfile.id,
      is_post_unlock: true,
      item_name: 'Unlock: '+ post.title,
      post_id: post.id
    }
  });

  logger.debug('unlock response: ', response.data);

  context.app.get('amplitudeClient').logEvent({
    event_type: eventNames.UNLOCK_CREATION,
    user_id: context.params.profile.id,
    event_properties: {
      creatorUsername: post.author.username,
      postId: post.id,
      priceValue: (post?.price ?? -1)/100
    }
  });


  context.result = {url: response.data};
  // record.id = context.data.postId;

  // record.chargeId = purchase.data.id;
  // record.creatorId = post.authorId;
} );

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.purchaserId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.purchaserId', allowUndefined: true }));


export default {
  before: {
    all: [],
    find: [filterAuthorized],
    get: [filterAuthorized],
    create: [filterAuthorized, associateAuthorized],
    update: [filterAuthorized, associateAuthorized],
    patch: [filterAuthorized, associateAuthorized],
    remove: [filterAuthorized, associateAuthorized]
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
