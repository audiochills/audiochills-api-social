// Initializes the `ProfileLikeComment` service on path `/profile-like-comment`
import { ServiceAddons } from '@feathersjs/feathers';
import { Application } from '../../declarations';
import { ProfileLikeComment } from './profile-like-comment.class';
import createModel from '../../models/profile-like-comment.model';
import hooks from './profile-like-comment.hooks';

// Add this service to the service type index
declare module '../../declarations' {
  interface ServiceTypes {
    'profile-like-comment': ProfileLikeComment & ServiceAddons<any>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/profile-like-comment', new ProfileLikeComment(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('profile-like-comment');

  service.hooks(hooks);
}
