// import { Hook, HookContext } from '@feathersjs/feathers';
import { hooks as sequelizeHooks } from 'feathers-sequelize';
import { authenticate } from '@feathersjs/authentication';
import { alterItems, iff, iffElse, paramsFromClient, isProvider } from 'feathers-hooks-common';
import { Op } from 'sequelize';
import sequelizeInclude from '../../hooks/sequelize-include';
import { setField } from 'feathers-authentication-hooks';


const includeCount = alterItems(async (record) => {
  record.setDataValue('likeCount', await record.countLikes());
  record.setDataValue('commentCount', await record.countComments());
  record.setDataValue('author', await record.getAuthor());
  record.setDataValue('genre', await record.getGenre());
  if (record.getDataValue('genre')) record.getDataValue('genre').setDataValue('category', await record.getDataValue('genre').getCategory());
});

const includeTipTotal = alterItems(async (record, context) => {

  const tips = await context.app.services['tip-post'].find({paginate:false, query:{ postId: record.getDataValue('id') }});

  const calculated = tips.reduce(
    (accumulator: any, currentValue: any) => (accumulator + currentValue.amountCents), 0
  );

  record.setDataValue('tipTotalCents', calculated  );
});

const filterPersonal = sequelizeInclude(({ models }, context) => [{
  model: models.profile,
  as: 'author',
  foreignKey: 'authorId',
  required: true,
  // attributes: [],
  where: { username: { [Op.ne]: null } },
  include: [{
    model: models.profile,
    through: { as: 'profileFollowProfile' },
    as: 'follower',
    foreignKey: 'followId',
    required: true, // => INNER JOIN
    attributes: [],
    where: { id: context.params?.profile.id },
  },
  ],
},{
  model: models.profile,
  as: 'likes',
  required: false,
  where: {id: context.params?.profile?.id}
}]);

const filterAll = sequelizeInclude(({ models }, context) => [{
  model: models.profile,
  as: 'author',
  foreignKey: 'authorId',
  required: true,
  // attributes: [],
  where: { username: { [Op.ne]: null } },
}]);


const initPost = alterItems( (record, context) => {
  context.params.sequelize = {
    raw: false,
    include: [
      {
        model: context.app.services['post-image']?.Model,
        raw: false
      },
      {
        model: context.app.services['post-audio']?.Model,
        raw: false
      }
    ],
  };
});

const didAuthenticatedLike = alterItems( (record ) => record.setDataValue('didLike', (record.get('likes')?.length > 0)));

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.authorId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.authorId', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [paramsFromClient('personal'), iffElse(context => context.params.personal, [iff(isProvider('external'), authenticate('api-key', 'oidc')), filterPersonal], [ filterAll])],
    get: [paramsFromClient('personal'), iffElse(context => context.params.personal, [iff(isProvider('external'), authenticate('api-key', 'oidc')), filterPersonal], [ filterAll])],
    create: [filterAuthorized, associateAuthorized, initPost],
    update: [filterAuthorized, associateAuthorized],
    patch: [filterAuthorized, associateAuthorized],
    remove: [filterAuthorized, associateAuthorized],
  },

  after: {
    all: [],
    find: [sequelizeHooks.hydrate(), includeCount, includeTipTotal, didAuthenticatedLike, sequelizeHooks.dehydrate()],
    get: [sequelizeHooks.hydrate(), includeCount, includeTipTotal, didAuthenticatedLike, sequelizeHooks.dehydrate()],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};

