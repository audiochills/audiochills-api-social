// Initializes the `post` service on path `/post`
import { Application } from '../../declarations';
import { Post } from './post.class';
import createModel from '../../models/post.model';
import hooks from './post.hooks';
import { ServiceAddons } from '@feathersjs/feathers';
import { PostAttributes } from '../../attributes';

declare module '../../declarations' {
  interface ServiceTypes {
    'post': Post & ServiceAddons<PostAttributes>;
  }
}

export default function (app: Application): void {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/post', new Post(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('post');
  service.hooks(hooks);
}
