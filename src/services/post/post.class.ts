import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { PostAttributes } from '../../attributes';
import { Application } from '../../declarations';

export class Post extends Service<PostAttributes> {
  app: Application;
  constructor(options: Partial<SequelizeServiceOptions>, app: Application) {
    super(options);
    this.app = app;
  }
}
