import { Service, SequelizeServiceOptions } from 'feathers-sequelize';
import { CommentAttributes } from '../../attributes';
import { Application } from '../../declarations';

export class Comment extends Service<CommentAttributes> {
  //eslint-disable-next-line @typescript-eslint/no-unused-vars
  constructor(options: Partial<SequelizeServiceOptions>, app: Application) {
    super(options);
  }
}
