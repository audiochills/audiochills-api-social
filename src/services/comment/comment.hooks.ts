import { hooks as sequelizeHooks } from 'feathers-sequelize';
import { alterItems } from 'feathers-hooks-common';
import validateSchema from '../../hooks/validate-schema';
import createSchema from '../../schema/create/comment.schema.json';
import processNestedSearch from '../../hooks/process-nested-search';
import { authenticate } from '@feathersjs/authentication/lib/hooks';
import { iff, isProvider } from 'feathers-hooks-common';
import { setField } from 'feathers-authentication-hooks';

const includeCount = alterItems(async (record) => {
  record.setDataValue('likeCount', await record.countLikes());
  record.setDataValue('commentCount', await record.countComments());
  record.setDataValue('author', await record.getAuthor());
});

const filterAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'params.query.authorId', allowUndefined: true }));
const associateAuthorized = iff(isProvider('external'), authenticate('api-key', 'oidc'), setField({ from: 'params.profile.id', as: 'data.authorId', allowUndefined: true }));

export default {
  before: {
    all: [],
    find: [processNestedSearch()],
    get: [processNestedSearch()],
    create: [filterAuthorized, associateAuthorized, validateSchema({ schema: createSchema })],
    update: [filterAuthorized, associateAuthorized],
    patch: [filterAuthorized, associateAuthorized],
    remove: [filterAuthorized, associateAuthorized],
  },

  after: {
    all: [],
    find: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    get: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    create: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    update: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    patch: [sequelizeHooks.hydrate(), includeCount, sequelizeHooks.dehydrate()],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
