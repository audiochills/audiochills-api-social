import { Service, HookContext } from '@feathersjs/feathers';
import { Application as Express } from '@feathersjs/express';

export interface ServiceTypes {}

export type Application = Express<ServiceTypes>;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export interface ApplicationHookContext<T = any, S = Service<T>> extends HookContext<T, S> {
  readonly app: Application
}
