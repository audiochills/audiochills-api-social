import { authenticate } from '@feathersjs/authentication';
import { Hook } from '@feathersjs/feathers';
import { every, iff, isNot, isProvider, PredicateFn } from 'feathers-hooks-common';

const isAuthenticationService: PredicateFn = ({ service, app }) => service as any === app.defaultAuthentication();

const checkAuthentication: Hook = iff(every(isNot(isAuthenticationService), isProvider('external')), authenticate('api-key', 'oidc'));

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [checkAuthentication],
    update: [checkAuthentication],
    patch: [checkAuthentication],
    remove: [checkAuthentication],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
