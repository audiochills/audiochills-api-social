import { Sequelize } from 'sequelize';
import { Application } from './declarations';
import logger from './logger';
import Umzug from 'umzug';

export default function (app: Application): void {

  const { migrations, seeders, ...options } = app.get('sequelize');

  const sequelize = new Sequelize({
    ...options,
    logging: (m) => logger.debug(m)
  });

  const oldSetup = app.setup;

  app.set('sequelizeClient', sequelize);

  app.setup = function (...args): Application {
    // Set up data relationships
    const models = sequelize.models;
    Object.keys(models).forEach((name) => {
      if ('associate' in models[name]) {
        (models[name] as any).associate(models);
      }
    });

    if (!args.length) return undefined;
    const result = oldSetup.apply(this, args);

    const [migrator, seeder] = [migrations, seeders].map(path => new Umzug({
      logging: (x: any) => logger.debug(x),
      migrations: { path, params: [sequelize.getQueryInterface(), Sequelize] },
      storage: 'sequelize',
      storageOptions: { sequelize },
    }));

    const createSync = async () => {
      logger.info('Sequelize migrations executing...');
      const migrationsExecuted = await migrator.up();
      logger.info(`Sequelize migrations executed (${migrationsExecuted.length})`);
      logger.info('Sequelize seeders executing...');
      const seeedersExecuted = await seeder.up();
      logger.info(`Sequelize seeders executed (${seeedersExecuted.length})`);
    };

    // Sync to the database
    app.set('sequelizeSync', createSync());

    return result;
  };
}
