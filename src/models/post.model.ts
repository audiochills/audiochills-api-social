// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';
import { PostAttributes } from '../attributes/post';


export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const post = sequelizeClient.define(
    'post',
    {
      id: {
        type: DataTypes.UUID,

        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      title: {
        type: DataTypes.STRING,

      },
      description: {
        type: DataTypes.TEXT,
      },
      status: {
        type: DataTypes.STRING,

      },
      price: {
        type: DataTypes.DOUBLE,
      },
      currency: {
        type: DataTypes.STRING,

        defaultValue: '$',
      },
      isNsfw: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      writerUsername: {
        type: DataTypes.STRING
      },
      customTags: {
        type: DataTypes.ARRAY(DataTypes.STRING)
      },
      isPaid: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      isPublished:{
        type: DataTypes.BOOLEAN,
        defaultValue: false
      }
    },
    {
      hooks: {
        beforeCount(options: any): HookReturn {
          options.raw = true;
        },
      },
    }
  );

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (post as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    post.belongsTo(models.profile, { as: 'author' });
    post.belongsToMany(models.profile, { through: models.profileLikePost, as: 'likes' });
    post.hasMany(models.comment);
    post.hasOne(models.postAudio);
    post.belongsToMany(models.profile, { through: models.purchasePost, as: 'purchasers' });
    post.hasMany(models.purchasePost);
    post.belongsTo(models.genre);
    post.hasOne(models.postImage);
    post.belongsToMany(models.profile, { through: models.tipPost, as: 'tippers' });
    post.hasMany(models.tipPost);
  };

  return post;
}
