// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';
import { CommentAttributes } from '../attributes/comment';

export interface CommentCreationAttributes extends Optional<CommentAttributes, 'id'> { }
export interface Comment extends Model<CommentAttributes, CommentCreationAttributes> { }

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const comment = sequelizeClient.define<Comment>(
    'comment',
    {
      id: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      message: {
        type: DataTypes.STRING,
      },
    },
    {
      hooks: {
        beforeCount(options: any): HookReturn {
          options.raw = true;
        }
      },
    }
  );

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (comment as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/

    comment.belongsTo(models.post);
    comment.belongsToMany(models.profile, {
      through:  models.profileLikeComment,
      as: 'likes',
    });
    comment.belongsTo(models.profile, {
      as: 'author',
    });
    comment.belongsTo(comment, {
      as: 'parent',
    });
    comment.hasMany(comment);
  };

  return comment;
}



