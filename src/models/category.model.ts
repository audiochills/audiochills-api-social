// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';
import { CategoryAttributes } from '../attributes/category';
import shortid from 'shortid';

export interface CategoryCreationAttributes extends Optional<CategoryAttributes, 'id'> { }
export interface Category extends Model<CategoryAttributes, CategoryCreationAttributes> { }

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const category = sequelizeClient.define<Category>(
    'category',
    {
      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4
      },
      slug: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: () => shortid.generate(),
      },
    },
    {
      hooks: {
        beforeCount(options: any): HookReturn {
          options.raw = true;
        },
      },
    }
  );

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (category as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    category.belongsToMany(models.profile, { through: models.profileHasCategory });
    category.hasMany(models.profileHasCategory);
    category.hasMany(models.genre);
  };

  return category;
}
