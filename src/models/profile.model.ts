// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';
import { ProfileAttributes } from '../attributes/profile';
import shortid from 'shortid';

export interface ProfileCreationAttributes extends Optional<ProfileAttributes, 'id'> { }
export interface Profile extends Model<ProfileAttributes, ProfileCreationAttributes> { }

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const profile = sequelizeClient.define<Profile>(
    'profile',
    {

      id: {
        type: DataTypes.UUID,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      oidcId: {
        type: DataTypes.STRING,
        unique: true,
      },
      username: {
        type: DataTypes.STRING,
        defaultValue: () => shortid.generate(),
        unique: true
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: () => shortid.generate(),
      },
      signupLevel: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
        validate: {
          min: 0,
          max: 3,
        },
      },
      percentageFee: {
        type: DataTypes.DOUBLE,
        allowNull: false,
        defaultValue: 0.15,
        validate: {
          min: 0,
          max: 0.99,
        },
      },
      isNsfw: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false
      },
      aboutTiktokUsername: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      aboutInstagramUsername: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      aboutHomepageUrl: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      aboutMessage: {
        type: DataTypes.TEXT,
        allowNull: true,
      },
      isCreatorAuthorized: {
        type: DataTypes.BOOLEAN,
        allowNull: true,
        defaultValue: false
      }
    },
    {
      hooks: {
        beforeCount(options: any): HookReturn {
          options.raw = true;
        },
      },
    }
  );

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (profile as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    // profile.belongsToMany(models.category, { through: 'ProfileHasCategory' });

    profile.hasOne(models.profileAvatar);
    profile.hasOne(models.profileBanner);
    profile.hasOne(models.profileAudio);
    profile.hasMany(models.profileHasCategory);
    profile.belongsToMany(models.category, { through: models.profileHasCategory });
    profile.hasMany(models.post, { foreignKey: 'authorId' });
    profile.hasMany(models.comment, { foreignKey: 'authorId' });
    profile.belongsToMany(models.post, { through: models.profileLikePost , foreignKey:'profileId', as: 'likes' });
    profile.belongsToMany(models.comment, { through: models.profileLikeComment , foreignKey:'profileId' });
    profile.belongsToMany(profile, {
      through: models.profileFollowProfile,
      as: 'follower',
      foreignKey: 'followId',
    });
    profile.belongsToMany(profile, {
      through: models.profileFollowProfile,
      as: 'follow',
      foreignKey: 'followerId',
    });

    // profile.belongsToMany(models.profile, {through: models.subscription, as: 'subscriber', foreignKey: 'creatorId'});
    // profile.belongsToMany(models.profile, {through: models.subscription, as: 'subscribed', foreignKey: 'fanId'});

    profile.belongsToMany(models.post, {as: 'bougthPost', through: models.purchasePost, foreignKey:'creatorId' });
    profile.belongsToMany(models.post, {as: 'soldPost', through: models.purchasePost, foreignKey:'fanId' });

    profile.hasOne(models.stripeInfo);
    profile.hasOne(models.plan);

    profile.belongsToMany(models.profile, {through: models.notification, as: 'notifier', foreignKey: 'notifiedId'});
    profile.belongsToMany(models.profile, {through: models.notification, as: 'notified', foreignKey: 'notifierId'});

    profile.hasMany(models.tipPost, {foreignKey:'tipperId'});


  };

  return profile;
}
