// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';
import { PlanAttributes } from '../attributes';


export interface PlanCreationAttributes extends Optional<PlanAttributes, 'id'> { }
export interface Plan extends Model<PlanAttributes, PlanCreationAttributes> { }

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const plan = sequelizeClient.define<Plan>(
    'plan', {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
	primaryKey: true
      },
      priceId: {
        type: DataTypes.STRING,
      },
      productId: {
        type: DataTypes.STRING
      },
      amountCents: {
        type: DataTypes.INTEGER
      },
      currency: {
        type: DataTypes.STRING,
        defaultValue: 'usd'
      }
    }, {
      hooks: {
        beforeCount(options: any): HookReturn {
          options.raw = true;
        }
      }
    });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (plan as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    plan.belongsTo(models.profile);
    plan.hasMany(models.subscription);
  };

  return plan;
}
