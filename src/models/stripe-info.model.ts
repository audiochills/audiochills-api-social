// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';
import { StripeInfoAttributes } from '../attributes/stripe-info';

export interface StripeInfoCreationAttributes extends Optional<StripeInfoAttributes, 'id'>{}
export interface StripeInfo extends Model<StripeInfoAttributes, StripeInfoCreationAttributes> { }


export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const stripeInfo = sequelizeClient.define<StripeInfo>(
    'stripeInfo', {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true
      },
      accountId:{
        type: DataTypes.STRING,
      },
      customerId: {
        type: DataTypes.STRING,
      },
      detailsSubmitted: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      chargesEnabled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      payoutsEnabled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
      },
      countryIso: {
        type: DataTypes.STRING
      },
      paymentMethodId: {
        type: DataTypes.STRING
      },
      priceId: {
        type: DataTypes.STRING
      },
      productId: {
        type: DataTypes.STRING
      }

    }, {
      hooks: {
        beforeCount(options: any): HookReturn {
          options.raw = true;
        }
      }
    });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (stripeInfo as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    stripeInfo.belongsTo(models.profile);
  };

  return stripeInfo;
}
