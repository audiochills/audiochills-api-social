// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const subscription = sequelizeClient.define('subscription', {
    id: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    isCancelled: {
      type: DataTypes.BOOLEAN,
    },
    renewalDate:{
      type: DataTypes.DATE,
    },
    stripeSubscriptionId: {
      type: DataTypes.STRING
    }

  }, {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (subscription as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    subscription.belongsTo(models.profile, { as: 'fan' });
    subscription.belongsTo(models.profile, { as: 'creator' });
    // subscription.belongsToMany(models.profile)
    subscription.belongsTo(models.plan);
  };

  return subscription;
}
