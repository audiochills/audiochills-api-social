// See http://docs.sequelizejs.com/en/latest/docs/models-definition/
// for more of what you can do here.
import { Sequelize, DataTypes, Model, Optional } from 'sequelize';
import { Application } from '../declarations';
import { HookReturn } from 'sequelize/types/lib/hooks';
import { NotificationAttributes } from '../attributes';

export interface NotificationCreationAttributes extends Optional<NotificationAttributes, 'id'> { }
export interface Notification extends Model<NotificationAttributes, NotificationCreationAttributes> { }

export default function (app: Application): typeof Model {
  const sequelizeClient: Sequelize = app.get('sequelizeClient');
  const notification = sequelizeClient.define('notification', {
    id: {
      type: DataTypes.UUID,
      primaryKey: true,
      defaultValue: DataTypes.UUIDV4
    },
    message: {
      type: DataTypes.STRING
    },
    isRead: {
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  },
  {
    hooks: {
      beforeCount(options: any): HookReturn {
        options.raw = true;
      }
    }
  });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  (notification as any).associate = function (models: any): void {
    // Define associations here
    // See http://docs.sequelizejs.com/en/latest/docs/associations/
    notification.belongsTo(models.profile, {as: 'notified', foreignKey: 'notifierId'});
    notification.belongsTo(models.profile, {as: 'notifier', foreignKey: 'notifiedId'});
  };

  return notification;
}
