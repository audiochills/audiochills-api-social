import { Application } from './declarations';
import * as Amplitude from '@amplitude/node';


export default async function (app: Application): Promise<void> {
  const oldSetup = app.setup;

  app.setup = function (...args): Application {
    const result = oldSetup.apply(this, args);

    const key = app.get('amplitude')['key'];
    const client = Amplitude.init(key);
    app.set('amplitudeClient', client);

    return result;
  };
}

export const eventNames = {
  BECOME_CREATOR: 'Become Creator',
  SEND_TIP: 'Send Tip Successful', // Done
  SUBSCRIPTION_SUCCESSFUL: 'Subscription Successful', // Done
  UNLOCK_CREATION: 'Unlock Successful', // Done
};
