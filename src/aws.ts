import { Application } from './declarations';
import AWS, { S3 } from 'aws-sdk';



export default async function (app: Application): Promise<void> {
  const oldSetup = app.setup;

  app.setup = function (...args): Application {
    const result = oldSetup.apply(this, args);

    const s3config = app.get('aws')['s3'];

    AWS.config.update({
      accessKeyId: s3config['accessKeyId'], // To hide these later
      secretAccessKey: s3config['secretAccessKey'],// To hide these later
      region: s3config['region']
    });


    app.set('clientS3', new S3());

    return result;
  };
}
