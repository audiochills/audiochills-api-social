import { PostAttributes } from './post';
import { ProfileAttributes } from './profile';

export interface CommentAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  message: string
  authorId?: string
  author?: ProfileAttributes
  postId?: string
  post?: PostAttributes
  parentId?: string
  parent?: CommentAttributes
}

declare module '.' {
  interface AttributeTypes {
    comment: CommentAttributes
  }
}
