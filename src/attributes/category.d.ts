import { ProfileAttributes } from './profile';

export interface CategoryAttributes {
  id: string
  slug: string
  name: string
  createdAt?: Date
  updatedAt?: Date
  profiles?: ProfileAttributes[]
}

declare module '.' {
  interface AttributeTypes {
    category: CategoryAttributes
  }
}
