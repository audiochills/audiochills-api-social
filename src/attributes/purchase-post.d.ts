import { PostAttributes } from './post';
import { ProfileAttributes } from './profile';


export interface PurchasePostAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  chargeId: string
  postId: string
  post?: PostAttributes
  creatorId: string
  creator?: ProfileAttributes
  fanId: string
  fan?: ProfileAttributes
}

declare module '.' {
  interface AttributeTypes {
    'purchase-post': PurchasePostAttributes
  }
}
