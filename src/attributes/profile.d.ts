import { CategoryAttributes } from './category';
import { ProfileAvatarAttributes } from './profile-avatar';
import { ProfileBannerAttributes } from './profile-banner';
import { ProfileAudioAttributes } from './profile-audio';
import { StripeInfoAttributes } from './stripe-info';


export interface ProfileAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  oidcId?: string
  username: string
  email?: string
  signupLevel: number
  percentageFee: number
  isNsfw: boolean
  categories?: Array<CategoryAttributes>
  name?: string
  aboutMessage?: string
  aboutHomepageUrl?: string
  aboutTiktokUsername?: string
  aboutInstagramUsername?: string
  currency?: string
  price?: number
  followerCount?: number
  isCreatorAuthorized?: boolean
  stripeInfo?: StripeInfoAttributes
  profileAvatar?: ProfileAvatarAttributes
  profileBanner?: ProfileBannerAttributes
  profileAudio?: ProfileAudioAttributes
}

declare module '.' {
  interface AttributeTypes {
    profile: ProfileAttributes
  }
}

