export interface PlanAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  profileId?: string
  priceId: string
  productId: string
  amountCents: number
  currency: string
}

declare module '.' {
  interface AttributeTypes {
    'plan': PlanAttributes
  }
}


