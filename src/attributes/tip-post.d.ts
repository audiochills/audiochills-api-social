import { PostAttributes } from './post';
import { ProfileAttributes } from './profile';


export interface TipPostAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  postId: string
  post?: PostAttributes
  tipperId: string
  tipper?: ProfileAttributes
  amountCents: number
}

declare module '.' {
  interface AttributeTypes {
    'tip-post': TipPostAttributes
  }
}
