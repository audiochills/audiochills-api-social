

export interface PostAudioAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  mimeType: string
  extension: string
  postId: string
  downloadUrl?: string
  uploadUrl?: string
  exists?: boolean
}

declare module '.' {
  interface AttributeTypes {
    'post-audio': PostAudioAttributes
  }
}
