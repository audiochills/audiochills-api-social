import { CommentAttributes } from './comment';
import { ProfileAttributes } from './profile';

export interface ProfileLikeCommentAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  commentId: string
  comment?: CommentAttributes
  profileId: string
  profile?: ProfileAttributes
}

declare module '.' {
  interface AttributeTypes {
    'profile-like-comment': ProfileLikeCommentAttributes
  }
}
