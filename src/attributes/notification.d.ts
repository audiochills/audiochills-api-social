import { ProfileAttributes } from './profile';

export interface NotificationAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  message: string
  notifierId: string
  notifier?: ProfileAttributes
  notifiedId: string
  notified?: ProfileAttributes
  isRead: boolean
}

declare module '.' {
  interface AttributeTypes {
    'notification': NotificationAttributes
  }
}
