import { ProfileAttributes } from './profile';


export interface SubscriptionAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  isCancelled: boolean
  renewalDate: Date
  creatorId: string
  creator?: ProfileAttributes
  fanId: string
  fan?: ProfileAttributes
  stripeSubscriptionId: string
}

declare module '.' {
  interface AttributeTypes {
    'subscription': SubscriptionAttributes
  }
}
