export interface ProfileAudioAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  mimeType: string
  extension: string
  profileId: string
  downloadUrl?: string
  uploadUrl?: string
  exists?: boolean
}

declare module '.' {
  interface AttributeTypes {
    'profile-audio': ProfileAudioAttributes
  }
}
