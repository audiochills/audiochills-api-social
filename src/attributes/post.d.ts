import { ProfileAttributes } from './profile';
import { GenreAttributes } from './genre';

export interface PostAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  title: string
  description: string
  status: string
  price: number
  currency: string
  isNsfw: boolean
  isPaid: boolean
  author?: ProfileAttributes
  authorId: string
  likeCount?: number
  commentCount?: number
  isPublished: boolean
  writerUsername?: string
  customTags?: Array<string>
  // sharedCount?: number
  genre?: GenreAttributes
  genreId?: string
}

declare module '.' {
  interface AttributeTypes {
    post: PostAttributes
  }
}
