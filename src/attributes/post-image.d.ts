
export interface PostImageAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  mimeType: string
  extension: string
  postId: string
  downloadUrl?: string
  uploadUrl?: string
  exists?: boolean
}

declare module '.' {
  interface AttributeTypes {
    'post-image': PostImageAttributes
  }
}
