import { ProfileAttributes } from './profile';

export interface ProfileFollowProfileAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  followerId?: string
  follower?: ProfileAttributes
  followId?: string
  follow?: ProfileAttributes
}

declare module '.' {
  interface AttributeTypes {
    'profile-follow-profile': ProfileFollowProfileAttributes
  }
}
