import { CategoryAttributes } from './category';

export interface GenreAttributes {
  id: string
  slug: string
  name: string
  createdAt?: Date
  updatedAt?: Date
  categoryId: string
  category?: CategoryAttributes
}

declare module '.' {
  interface AttributeTypes {
    genre: GenreAttributes
  }
}
