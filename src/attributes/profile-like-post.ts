import { PostAttributes } from './post';
import { ProfileAttributes } from './profile';

export interface ProfileLikePostAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  postId: string
  post?: PostAttributes
  profileId: string
  profile?: ProfileAttributes
}

declare module '.' {
  interface AttributeTypes {
    'profile-like-post': ProfileLikePostAttributes
  }
}
