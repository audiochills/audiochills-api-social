import { ProfileAttributes } from './profile';


export interface StripeInfoAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  accountId: string
  customerId: string
  profileId?: string
  detailsSubmitted: boolean
  chargesEnabled: boolean
  payoutsEnabled: boolean
  countryIso: string
  paymentMethodId: string
  priceId: string
  productId: string
  createAccount?: boolean
  profile?: ProfileAttributes
  url?: string
}

declare module '.' {
  interface AttributeTypes {
    'stripe-info': StripeInfoAttributes
  }
}
