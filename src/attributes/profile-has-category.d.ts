import { CategoryAttributes } from './category';
import { ProfileAttributes } from './profile';

export interface ProfileHasCategoryAttributes {
  id: string
  createdAt?: Date
  updatedAt?: Date
  profile?: ProfileAttributes
  profileId?: string
  category?: CategoryAttributes
  categoryId?: string
}

declare module '.' {
  interface AttributeTypes {
    'profile-has-category': ProfileHasCategoryAttributes
  }
}
