import { Hook, HookContext } from '@feathersjs/feathers';
import { Includeable, Sequelize } from 'sequelize';

/**
 * Apply include options to sequelize queries. https://github.com/feathersjs-ecosystem/feathers-sequelize#setting-paramssequelizeinclude
 *
 * @param include - A function which returns the include option to use.
 */
export default (include: (sequelize: Sequelize, ctx: HookContext) => Includeable | Includeable[] | undefined): Hook => async (context) => {
  const sequelize = context.app.get('sequelizeClient') as Sequelize;
  const includeOptions = await include(sequelize, context) as ConcatArray<never>;
  context.params.sequelize = context.params.sequelize || {};
  context.params.sequelize.include = [].concat(context.params.sequelize.include || []).concat(includeOptions || []);
};
