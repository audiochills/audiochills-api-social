// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';


// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options = {}): Hook => {
  return async (context: HookContext): Promise<HookContext> => {


    const include = context.params.query?.include;
    const query = context.params.query?.query;


    if (include) {
      const parsedJson = JSON.parse(include) as any;
      const sequelizeJson = {
        raw: false,
        include: [
          {
            model: context.app.services[parsedJson?.model]?.Model,
            as: parsedJson?.as,
            raw: false
          },
        ],
      };

      if (!parsedJson.as) {
        sequelizeJson.include[0].as = undefined;
      }




      context.params.sequelize = sequelizeJson;
      // Update the query to not include `include`
      context.params.query = query;
    }

    return context;
  };
};
