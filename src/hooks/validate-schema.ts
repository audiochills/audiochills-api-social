// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';
import Ajv, { AnySchema } from 'ajv';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export default (options: { schema: AnySchema }): Hook => {
  return async (context: HookContext): Promise<HookContext> => {
    return context;
    const validate = new Ajv().compile(options.schema);
    if (validate(context.data)) {
      return context;
    }
    throw new Error(validate.errors?.[0].message);
  };
};
