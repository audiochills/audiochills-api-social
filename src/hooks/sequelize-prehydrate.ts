import { Hook } from '@feathersjs/feathers';

/**
 * Apply include options to sequelize queries. https://github.com/feathersjs-ecosystem/feathers-sequelize#setting-paramssequelizeinclude
 *
 * @param include - A function which returns the include option to use.
 */
export default (): Hook => async (context) => {
  context.params.sequelize = context.params.sequelize || {};
  context.params.sequelize.raw = false;
};
