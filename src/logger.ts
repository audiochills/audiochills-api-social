import { createLogger, format, transports } from 'winston';
import config from 'config';

// Configure the Winston logger. For the complete documentation see https://github.com/winstonjs/winston
const logger = createLogger({
  // To see more detailed errors, change this to 'debug'
  ...config.get('logger'),
  format: format.combine(
    format.colorize({ level: true }),
    format.splat(),
    format.simple(),
  ),
  transports: [
    new transports.Console()
  ],
});

export default logger;
