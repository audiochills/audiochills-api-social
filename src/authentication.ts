import { AuthenticationService } from '@feathersjs/authentication';
import { Application } from './declarations';
import { OidcStrategy } from 'feathers-authentication-oidc';
import { ApiKeyStrategy } from '@thesinding/authentication-api-key';

export default function (app: Application): void {
  const authentication = new AuthenticationService(app);
  authentication.register('oidc', new OidcStrategy());
  authentication.register('api-key', new ApiKeyStrategy());

  app.set('authenticationService', authentication);
  app.use('/authentication', authentication);
}
