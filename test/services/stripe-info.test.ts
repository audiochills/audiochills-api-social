import app from '../../src/app';

describe('\'stripeInfo\' service', () => {
  it('registered the service', () => {
    const service = app.service('stripe-info');
    expect(service).toBeTruthy();
  });
});
