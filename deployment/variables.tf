variable "docker_registry_name" {
  type = string
}
variable "docker_registry_username" {
  type = string
}
variable "docker_registry_password" {
  type = string
}
variable "docker_registry_image_tag" {
  type = string
}
variable "docker_registry_image_repository" {
  type = string
}
variable "node_env" {
  type = string
}
variable "node_domain" {
  type = string
}
variable "node_auth_enabled" {
  type    = bool
  default = false
}
variable "node_auth_username" {
  type    = string
  default = "video"
}
variable "node_auth_password" {
  type    = string
  default = "sucks"
}
variable "kube_annotations" {
  type    = map(string)
  default = {}
}
variable "kube_namespace" {
  type    = string
  default = "default"
}
