FROM node:14-alpine
WORKDIR /app
EXPOSE 5000

COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn

COPY . .
RUN yarn build

CMD ["node", "lib/"]
