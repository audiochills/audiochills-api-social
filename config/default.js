module.exports = {
  host: '0.0.0.0',
  port: 5000,
  public: '../public/',
  paginate: {
    default: 10,
    max: 50
  },
  sequelize: {
    dialect: 'postgres',
    host: 'localhost',
    username: 'audiochills',
    password: 'audiochills',
    database: 'audiochills',
    logging: true,
    define: {
      freezeTableName: true
    },
    migrations: '../migrations/scripts',
    seeders: '../migrations/seeders'
  },
  logger: {
    level: 'debug'
  },
  helmet: {
    contentSecurityPolicy: false,
  },
  cors: {
    origin: '*',
  },

  swagger: {
    uiIndex: true,
    openApiVersion: 3,
    docsPath: '/',
    defaults: {
      operations: {
        all: {
          security: [
            { 'api-key': [] },
            { oidc: [] }
          ]
        }
      }
    },
    specs: {
      info: {
        title: 'AudioChills Social API',
        description: 'REST API for the AudioChills Social Database',
        version: '1.0.0',
      },
      components: {
        securitySchemes: {
          oidc: {
            type: 'openIdConnect',
            openIdConnectUrl: 'https://staging.signin.audiochills.art/auth/realms/master/.well-known/openid-configuration',
          },
          'api-key': {
            type: 'apiKey',
            in: 'header',
            name: 'x-api-key'
          },
        },
      },
    },
  },

  aws: {
    s3: {
      accessKeyId: 'AKIA6AHMOCDPIO2VVY6F',
      secretAccessKey: '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',
      region: 'us-east-1'
    },
    s3Presign: {
      bucket: 'audiochills.audio-development',
    }
  },
  authentication: {
    secret: 'dummy',
    entity: 'profile',
    service: 'profile',
    authStrategies: ['api-key', 'oidc'],
    oidc: {
      issuer: 'https://staging.signin.audiochills.art/auth/realms/audiochills',
    },
    'api-key': {
      key: 'audiochills-api-payments:XCGiyXnCM93Uj36niDet4AvYEeVp4pK898FQL4HNF4Dkp8MStXAprL2aR4RXRaMt',
      headerField: 'x-api-key',
    }
  },

  stripe: {
    endpoint: 'https://626cceo71b.execute-api.us-east-1.amazonaws.com/dev',
    endpoint_temp: 'http:/localhost:5000'
  },

  amplitude: {
    key: '17c3a1a20cfc1e3597dff46e1652725b'
  }
};
